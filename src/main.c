//
// Created by kamil on 12.12.2023.
//
#define _GNU_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define DEFAULT_HEAP_SIZE 4096

void debug(const char* fmt, ... );

#define get_header(mem) \
    ((struct block_header*) (((uint8_t*) (mem)) - offsetof(struct block_header, contents)))

static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

void testing_malloc(){
    printf("Test 1: Usual malloc\n");

    printf("trying to allocate %d bytes\n", 993);
    void* first = _malloc(993);
    assert(first);

    _free(first);
    printf("Test 1 is OK.\n");
    printf("---------------------------------------------------\n");
}

void testing_free_one_of_some() {
    printf("Test 2: Free one of some blocks\n");

    printf("trying to allocate %d bytes\n", 993);
    void* first = _malloc(993);

    printf("trying to allocate %d bytes\n", 7);
    void* second = _malloc(7);

    printf("trying to allocate %d bytes\n", 7);
    void* third = _malloc(7);

    assert(first != NULL);
    assert(second != NULL);
    assert(third != NULL);

    printf("trying to free first block\n");

    _free(first);

    assert(get_header(first)->is_free && !get_header(second)->is_free && !get_header(third)->is_free);

    _free(second);
    _free(third);

    printf("Test 2 is OK.\n");
    printf("---------------------------------------------------\n");
}

void testing_free_two_of_some() {
    printf("Test 3: Free two of some blocks\n");

    printf("trying to allocate %d bytes\n", 993);
    void* first = _malloc(993);

    printf("trying to allocate %d bytes\n", 7);
    void* second = _malloc(7);

    printf("trying to allocate %d bytes\n", 7);
    void* third = _malloc(7);

    assert(first != NULL);
    assert(second != NULL);
    assert(third != NULL);

    printf("trying to free first block\n");
    _free(first);

    printf("trying to free second block\n");
    _free(second);

    assert(get_header(first)->is_free && get_header(second)->is_free && !get_header(third)->is_free);

    _free(third);

    printf("Test 3 is OK.\n");
    printf("---------------------------------------------------\n");
}

void testing_union_regions(void* heap) {
    struct region* region = (struct region*) heap;
    printf("Test 4: Expansion of territory\n");

    size_t initial_region_size = region->size;

    void* allocated = _malloc(7 * 4096);
    size_t union_region_size = region->size;

    assert(initial_region_size < union_region_size);

    _free(allocated);

    printf("Test 4 is OK.\n");
    printf("---------------------------------------------------\n");
}

void testing_advanced_union_regions() {
    printf("Test 5: Advanced expansion of territory\n");

    void* pre_allocated = map_pages((const void *)HEAP_START, 7, MAP_FIXED);

    void* post_allocated = _malloc(993);

    assert(pre_allocated != post_allocated);

    _free(post_allocated);

    printf("Test 5 is OK.\n");
    printf("---------------------------------------------------\n");
}

int main() {
    void* heap = heap_init(DEFAULT_HEAP_SIZE);
    assert(heap);

    printf("Tests!!!!!!!!!\n");
    testing_malloc();
    testing_free_one_of_some();
    testing_free_two_of_some();
    testing_union_regions(heap);
    testing_advanced_union_regions();

    printf("All tests are OK. It's amazing!");
}